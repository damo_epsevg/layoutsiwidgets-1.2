// 1.2

// Un text fix
// Introduim el contingut del text com a literal

// OBSERVACIONS
//  1. El new TextView s'ha de fer un cop l'activitat està creada
//  2. El XML no el fem servir pera res

package edu.upc.cs.damo;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;


public class activitat extends Activity {
    /** Called when the activity is first created. */
	
	TextView text;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        text = new TextView(this);
    	text.setText("Hola a tothom");
        setContentView(text);
    }
}
